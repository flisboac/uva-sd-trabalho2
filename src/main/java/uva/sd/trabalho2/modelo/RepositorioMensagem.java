package uva.sd.trabalho2.modelo;

public interface RepositorioMensagem extends AutoCloseable {

    void salvar(Mensagem mensagem) throws ErroPersistencia;

    @Override
    void close();
}
