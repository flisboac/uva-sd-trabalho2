package uva.sd.trabalho2.modelo;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public enum TipoMensagem {

	ERRO_PARCIAL(false),
	ERRO_TOTAL(true),
	ALERTA_NIVEL_1(false),
	ALERTA_NIVEL_2(false);

	private boolean prioritario;

    TipoMensagem(boolean prioritario) {
        this.prioritario = prioritario;
    }

    public String getNome() {
        return name();
    }

    public boolean isPrioritario() {
        return prioritario;
    }

    public static Optional<TipoMensagem> buscarPorNome(String nome) {
    	for (TipoMensagem elem : values()) if (elem.name().equals(nome)) return Optional.of(elem);
    	return Optional.empty();
    }

    public static List<String> getNomesPrioritarios() {
        return Arrays.stream(values())
                .filter(TipoMensagem::isPrioritario)
                .map(TipoMensagem::getNome)
                .collect(Collectors.toList());
    }
}
