package uva.sd.trabalho2.modelo.impl;

public class Repositorios {

    private static final String nomeArquivoGenerico = "log.txt";
    private static final String nomeArquivoCritico = "log-critico.txt";

    private static RepositorioMensagemArquivo repositorioMensagemGenerico;
    private static RepositorioMensagemArquivo repositorioMensagemCritico;

    public static RepositorioMensagemArquivo getRepositorioMensagemGenerico() {
        if (repositorioMensagemGenerico == null) repositorioMensagemGenerico = new RepositorioMensagemArquivo(nomeArquivoGenerico);
        return repositorioMensagemGenerico;
    }

    public static RepositorioMensagemArquivo getRepositorioMensagemPrioritario() {
        if (repositorioMensagemCritico == null) repositorioMensagemCritico = new RepositorioMensagemArquivo(nomeArquivoCritico);
        return repositorioMensagemCritico;
    }
}
