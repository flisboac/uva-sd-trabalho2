package uva.sd.trabalho2.modelo.impl;

import uva.sd.trabalho2.modelo.ErroPersistencia;
import uva.sd.trabalho2.modelo.Mensagem;
import uva.sd.trabalho2.modelo.RepositorioMensagem;
import uva.sd.trabalho2.servicos.impl.Mensagens;

import java.io.*;

public class RepositorioMensagemArquivo implements RepositorioMensagem {

	private String nomeArquivo;
	private PrintWriter writer;

	public RepositorioMensagemArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
	
	private void verificarWriter() {
		if (this.writer == null) {
			File arquivo = new File(nomeArquivo);
			
			try {
			    FileOutputStream rawStream = new FileOutputStream(arquivo, true); // append:true
			    OutputStreamWriter streamWriter = new OutputStreamWriter(rawStream, "UTF-8");
				writer = new PrintWriter(streamWriter, true); // auto-flush:true
				
			} catch (IOException e) {
				throw new ErroPersistencia(e);
			}
		}
	}
	
	@Override
    public void salvar(Mensagem mensagem) {
		verificarWriter();
		String linha = Mensagens.serializar(mensagem);
		this.writer.println(linha);
	}

	@Override
	public void close() {
		if (this.writer != null) this.writer.close();	
	}
}
