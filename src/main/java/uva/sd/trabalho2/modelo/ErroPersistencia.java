package uva.sd.trabalho2.modelo;

import uva.sd.trabalho2.lang.Erro;

public class ErroPersistencia extends Erro {
    public ErroPersistencia() {
    }

    public ErroPersistencia(String s) {
        super(s);
    }

    public ErroPersistencia(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ErroPersistencia(Throwable throwable) {
        super(throwable);
    }

    public ErroPersistencia(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
