package uva.sd.trabalho2.modelo;

import uva.sd.trabalho2.lang.Erro;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.time.ZonedDateTime;

public class Mensagem {

	private TipoMensagem tipo;
	private String ipOrigem;
	private ZonedDateTime dataHora;
	private String mensagem;

	Mensagem(TipoMensagem tipoMensagem, String mensagem) throws UnknownHostException {
        this(tipoMensagem, InetAddress.getLocalHost().getHostAddress(), ZonedDateTime.now(), mensagem);
	}

	public Mensagem(TipoMensagem tipo, String ipOrigem, ZonedDateTime dataHora, String mensagem) {
		this.tipo = tipo;
		this.ipOrigem = ipOrigem;
		this.dataHora = dataHora;
		this.mensagem = mensagem;
	}

	public TipoMensagem getTipo() {
		return tipo;
	}
	
	public String getIpOrigem() {
		return ipOrigem;
	}

	public ZonedDateTime getDataHora() {
		return dataHora;
	}
	
	public String getMensagem() {
		return mensagem;
	}

    public static Mensagem criar(TipoMensagem tipo, String mensagem) {
        try {
            return new Mensagem(tipo, mensagem);
        } catch (UnknownHostException e) {
            throw new Erro(e);
        }
    }
}
