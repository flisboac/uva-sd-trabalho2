package uva.sd.trabalho2.lang;

public class Erro extends RuntimeException {
    public Erro() {
    }

    public Erro(String s) {
        super(s);
    }

    public Erro(String s, Throwable throwable) {
        super(s, throwable);
    }

    public Erro(Throwable throwable) {
        super(throwable);
    }

    public Erro(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
