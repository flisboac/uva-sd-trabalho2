package uva.sd.trabalho2.aplicacao;

import uva.sd.trabalho2.modelo.Mensagem;
import uva.sd.trabalho2.modelo.TipoMensagem;
import uva.sd.trabalho2.servicos.Mensageria;
import uva.sd.trabalho2.servicos.ProdutorMensagem;

import java.util.Arrays;

public class CliMonitor extends CliBase {

    private static enum OpcaoPrincipal {
        INVALIDO(null, null),
        SINALIZAR_EVENTO(1, "Sinalizar evento"),
        SAIR(9, "SAIR");

        public final Integer numero;
        public final String descricao;

        OpcaoPrincipal(Integer numero, String descricao) {
            this.numero = numero;
            this.descricao = descricao;
        }

        public boolean isValido() {
            return this.numero != null;
        }

        public static OpcaoPrincipal buscarPorId(int idOpcao) {
            return Arrays.stream(values())
                    .filter((operacao) -> operacao.numero != null && operacao.numero.equals(idOpcao))
                    .findAny()
                    .orElse(INVALIDO);
        }
    }

    private static enum OpcaoSinalizarEvento {
        INVALIDO(null, null, null),
        ERRO_PARCIAL(1, "Erro parcial", TipoMensagem.ERRO_PARCIAL),
        ERRO_TOTAL(2, "Erro total", TipoMensagem.ERRO_TOTAL),
        ALERTA_NIVEL_1(3, "Alerta (Nível 1)", TipoMensagem.ALERTA_NIVEL_1),
        ALERTA_NIVEL_2(4, "Alerta (Nível 2)", TipoMensagem.ALERTA_NIVEL_2),
        VOLTAR(9, "Voltar", null);

        public static final String NOME_SECAO = "SINALIZAR EVENTO";

        public final Integer numero;
        public final String descricao;
        public final TipoMensagem tipoMensagem;

        OpcaoSinalizarEvento(Integer numero, String descricao, TipoMensagem tipoMensagem) {
            this.numero = numero;
            this.descricao = descricao;
            this.tipoMensagem = tipoMensagem;
        }

        public boolean isValido() {
            return this.numero != null;
        }

        public static OpcaoSinalizarEvento buscarPorId(int idOpcao) {
            return Arrays.stream(values())
                    .filter((operacao) -> operacao.numero != null && operacao.numero.equals(idOpcao))
                    .findAny()
                    .orElse(INVALIDO);
        }
    }

    private ProdutorMensagem produtor;

    public CliMonitor(Mensageria mensageria) {
        super(ModoExecucao.MONITOR, mensageria);
    }

    @Override
    protected void inicializar() {
        super.inicializar();
        this.produtor = this.mensageria.criarProdutor();
    }

    @Override
    protected void finalizar() {
        if (this.produtor != null) this.produtor.close();
        super.finalizar();
    }

    @Override
    protected void executar() throws Exception {
        OpcaoPrincipal opcao = OpcaoPrincipal.INVALIDO;

        imprimirMensagem();
        while (opcao != OpcaoPrincipal.SAIR) {
            imprimirMensagem("-=[[ MONITOR DE EVENTOS (SIMULADOR DE EVENTOS, na verdade) ]]=-");
            imprimirMensagem("MENU PRINCIPAL");
            Arrays.stream(OpcaoPrincipal.values())
                    .filter(OpcaoPrincipal::isValido)
                    .forEach(this::imprimirOpcao);
            imprimirMensagem("Digite uma opção: ");
            int idOpcao = lerInt();
            opcao = OpcaoPrincipal.buscarPorId(idOpcao);

            switch (opcao) {
            case SINALIZAR_EVENTO:
                doSinalizarEvento();
                break;
            case SAIR:
                break;
            case INVALIDO:
                imprimirMensagem("Opção inválida. Tente novamente.");
                break;
            }
        }
    }

    private void doSinalizarEvento() {
        OpcaoSinalizarEvento opcao = OpcaoSinalizarEvento.INVALIDO;

        while (opcao != OpcaoSinalizarEvento.VOLTAR) {
            imprimirMensagem("MENU: " + OpcaoSinalizarEvento.NOME_SECAO);
            Arrays.stream(OpcaoSinalizarEvento.values())
                    .filter(OpcaoSinalizarEvento::isValido)
                    .forEach(this::imprimirOpcaoSinalizarEvento);
            imprimirMensagem("Digite uma opção: ");
            int idOpcao = lerInt();
            opcao = OpcaoSinalizarEvento.buscarPorId(idOpcao);

            switch (opcao) {
            case INVALIDO:
                imprimirMensagem("Opção inválida. Tente novamente.");
                break;
            case VOLTAR:
                break;
            case ERRO_PARCIAL:
            case ERRO_TOTAL:
            case ALERTA_NIVEL_1:
            case ALERTA_NIVEL_2:
                imprimirMensagem("Digite o texto do evento: ");
                String texto = lerLinha();
                Mensagem mensagem = Mensagem.criar(opcao.tipoMensagem, texto);
                produtor.enviar(mensagem);
                imprimirMensagem("Mensagem enviada com sucesso.");
                opcao = OpcaoSinalizarEvento.VOLTAR;
                break;
            }
        }
    }

    private void imprimirOpcao(OpcaoPrincipal operacao) {
        imprimirMensagem("(%d) %s", operacao.numero, operacao.descricao);
    }

    private void imprimirOpcaoSinalizarEvento(OpcaoSinalizarEvento opcao) {
        imprimirMensagem("(%d) %s", opcao.numero, opcao.descricao);
    }
}
