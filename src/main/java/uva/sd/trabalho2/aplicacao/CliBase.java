package uva.sd.trabalho2.aplicacao;

import uva.sd.trabalho2.servicos.Mensageria;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.Optional;
import java.util.Scanner;

public abstract class CliBase implements Runnable {

    protected final Mensageria mensageria;

    protected InputStream in = System.in;
    protected PrintStream out = System.out;
    protected Scanner inScanner;
    protected final ModoExecucao modoExecucao;

    protected abstract void executar() throws Exception;

    public CliBase(ModoExecucao modoExecucao, Mensageria mensageria) {
        this.modoExecucao = modoExecucao;
        this.mensageria = mensageria;
    }

    @Override
    public void run() {
        try {
            inicializar();
            imprimirMensagem("Aplicação iniciada em modo %s.", modoExecucao.getNome());
            executar();

        } catch (Exception e) {
            e.printStackTrace(out);

        } finally {
            finalizar();
        }
    }

    protected void inicializar() {
        inScanner = new Scanner(in);
    }

    protected void finalizar() {
        if (inScanner != null) inScanner.close();
    }

    protected void imprimirMensagem() {
        imprimirMensagem("");
    }

    protected void imprimirMensagem(String formato, Object... argumentos) {
        String mensagem = String.format(formato, argumentos);
        out.println(mensagem);
    }

    protected String lerLinha() {
        return inScanner.nextLine();
    }

    protected int lerInt() {
        Integer intg = null;

        while (intg == null) {
            try {
                intg = inScanner.nextInt();
                inScanner.nextLine();

            } catch (InputMismatchException e) {
                inScanner.nextLine();
                out.println("*ERRO: Por favor, digite um número inteiro.");
            }
        }

        return intg;
    }

}
