package uva.sd.trabalho2.aplicacao;

import uva.sd.trabalho2.modelo.Mensagem;
import uva.sd.trabalho2.modelo.RepositorioMensagem;
import uva.sd.trabalho2.modelo.impl.Repositorios;
import uva.sd.trabalho2.servicos.ConsumidorMensagem;
import uva.sd.trabalho2.servicos.Mensageria;
import uva.sd.trabalho2.servicos.ProdutorMensagem;
import uva.sd.trabalho2.servicos.impl.Mensagens;

public class CliLogger extends CliBase implements RepositorioMensagem {

    private ConsumidorMensagem consumidor;
    private RepositorioMensagem repositorio;

    public CliLogger(ModoExecucao modoExecucao, Mensageria mensageria) {
        super(modoExecucao, mensageria);
        assert modoExecucao.isLogger();
    }

    @Override
    protected void inicializar() {
        super.inicializar();
        switch (this.modoExecucao) {
        case LOGGER_GENERICO:
            this.repositorio = Repositorios.getRepositorioMensagemGenerico();
            this.consumidor = this.mensageria.criarConsumidorGenerico(this);
            break;
        case LOGGER_PRIORIDADE:
            this.repositorio = Repositorios.getRepositorioMensagemPrioritario();
            this.consumidor = this.mensageria.criarConsumidorPrioritario(this);
            break;
        default:
            throw new RuntimeException("Modo de execução inválido.");
        }
    }

    @Override
    protected void finalizar() {
        if (this.consumidor != null) this.consumidor.close();
        super.finalizar();
    }

    @Override
    protected void executar() throws Exception {

        imprimirMensagem();
        imprimirMensagem("* INFO: Logger iniciado. Pressione 'X' e depois '<ENTER>' a qualquer momento para fechar.");
        char c = '\0';
        while (c != 'x' && c != 'X') {
            c = this.inScanner.next(".").charAt(0);
        }
    }

    @Override
    public void salvar(Mensagem mensagem) {
        String strMensagem = Mensagens.serializar(mensagem);
        this.out.println(strMensagem);
        this.repositorio.salvar(mensagem);
    }

    @Override
    public void close() {
        finalizar();
    }
}
