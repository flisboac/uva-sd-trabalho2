package uva.sd.trabalho2.aplicacao;

import java.util.Arrays;
import java.util.Optional;

public enum ModoExecucao {

    LOGGER_GENERICO("generico", true),
    LOGGER_PRIORIDADE("prioridade", true),
    MONITOR("monitor", false);

    private final boolean logger;
    private final String nome;

    private ModoExecucao(String nome, boolean logger) {
        this.nome = nome;
        this.logger = logger;
    }

    public String getNome() {
        return nome;
    }

    public boolean isLogger() {
        return logger;
    }

    public static Optional<ModoExecucao> buscarPorNome(String nome) {
        return Arrays.stream(values()).filter(modoExecucao -> modoExecucao.getNome().equals(nome)).findAny();
    }
}
