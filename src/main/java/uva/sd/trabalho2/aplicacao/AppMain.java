package uva.sd.trabalho2.aplicacao;

import uva.sd.trabalho2.servicos.Mensageria;

import java.util.Optional;

public class AppMain {

	public static void main(String[] args) {
	    String strModo = args[0];
	    Optional<ModoExecucao> optModo = ModoExecucao.buscarPorNome(strModo);

	    if (!optModo.isPresent()) {
	        System.err.println("*ERRO: Modo de execução '" + strModo + "' desconhecido.");
	        System.exit(1);
        }

		try (Mensageria mensageria = new Mensageria()) {
            switch (optModo.get()) {
            case LOGGER_GENERICO:
            case LOGGER_PRIORIDADE:
                new CliLogger(optModo.get(), mensageria).run();
                break;
            case MONITOR:
                new CliMonitor(mensageria).run();
                break;
            }
		}
	}

}
