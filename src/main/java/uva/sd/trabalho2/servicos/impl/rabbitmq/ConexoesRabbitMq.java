package uva.sd.trabalho2.servicos.impl.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import uva.sd.trabalho2.servicos.ErroMensageria;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public final class ConexoesRabbitMq {

    private static ConnectionFactory factory;
    private static Connection connection;

    public static ConnectionFactory getConnectionFactory() {
        if (factory == null) {
            factory = new ConnectionFactory();
            factory.setHost("localhost");
        }
        return factory;
    }

    public static Connection criarConexao() {
        try {
            return getConnectionFactory().newConnection();
        } catch (IOException | TimeoutException e) {
            throw new ErroMensageria(e);
        }
    }
}
