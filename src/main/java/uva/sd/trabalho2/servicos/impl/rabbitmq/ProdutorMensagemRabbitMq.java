package uva.sd.trabalho2.servicos.impl.rabbitmq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import uva.sd.trabalho2.modelo.Mensagem;
import uva.sd.trabalho2.servicos.ConfiguracaoMensagens;
import uva.sd.trabalho2.servicos.ErroMensageria;
import uva.sd.trabalho2.servicos.ProdutorMensagem;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ProdutorMensagemRabbitMq implements ProdutorMensagem {

    private final ConfiguracaoMensagens configuracao;
    private final Channel channel;

    public ProdutorMensagemRabbitMq(Channel channel, ConfiguracaoMensagens configuracao) {
        this.channel = channel;
        this.configuracao = configuracao;
    }

    @Override
    public void enviar(Mensagem mensagem) {
        byte[] bytes = MensagensRabbitMq.serializar(mensagem);
        String exchangeName = this.configuracao.getNomeTopico();
        String broadcastExchangeName = this.configuracao.getNomeTopico();
        String bindingKey = MensagensRabbitMq.getChaveRedirecionamento(mensagem);
        AMQP.BasicProperties basicProperties = MensagensRabbitMq.getBasicProperties();

        enviar(exchangeName, bindingKey, basicProperties, bytes);
        enviar(exchangeName, "", basicProperties, bytes);
    }

    private void enviar(String exchangeName, String bindingKey, AMQP.BasicProperties basicProperties, byte[] bytes) {
        try {
            channel.basicPublish(exchangeName, bindingKey, basicProperties, bytes);
        } catch (IOException e) {
            throw new ErroMensageria(e);
        }
    }

    @Override
    public void close() {
        try {
            this.channel.close();
        } catch (IOException | TimeoutException e) {
            throw new ErroMensageria(e);
        }
    }
}
