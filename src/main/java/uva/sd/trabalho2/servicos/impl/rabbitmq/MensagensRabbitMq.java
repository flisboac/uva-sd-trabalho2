package uva.sd.trabalho2.servicos.impl.rabbitmq;

import com.rabbitmq.client.AMQP;
import uva.sd.trabalho2.modelo.Mensagem;
import uva.sd.trabalho2.servicos.ErroMensageria;
import uva.sd.trabalho2.servicos.impl.Mensagens;

import java.io.UnsupportedEncodingException;

public class MensagensRabbitMq {

    public static AMQP.BasicProperties getBasicProperties() {
        return new AMQP.BasicProperties.Builder()
                .contentEncoding("UTF-8")
                .contentType("text/plain")
                .build();
    }

    private static String paraString(byte[] conteudo) {
        try {
            return new String(conteudo, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new ErroMensageria(e);
        }
    }

    private static byte[] paraBytes(String conteudo) {
        try {
            return conteudo.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new ErroMensageria(e);
        }
    }

    public static byte[] serializar(Mensagem mensagem) {
        return paraBytes(Mensagens.serializar(mensagem));
    }

    public static Mensagem deserializar(byte[] dados) {
        return Mensagens.deserializar(paraString(dados));
    }

    public static String getChaveRedirecionamento(Mensagem mensagem) {
        return mensagem.getTipo().getNome();
    }
}
