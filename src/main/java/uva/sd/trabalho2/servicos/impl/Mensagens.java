package uva.sd.trabalho2.servicos.impl;

import uva.sd.trabalho2.modelo.Mensagem;
import uva.sd.trabalho2.modelo.TipoMensagem;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Mensagens {

    private static DateTimeFormatter getDateTimeFormatter() {
        return DateTimeFormatter.ISO_OFFSET_DATE_TIME;
    }

    public static String serializar(Mensagem mensagem) {
        String dataHora = getDateTimeFormatter().format(mensagem.getDataHora());
        String ip = mensagem.getIpOrigem();
        String tipoEvento = mensagem.getTipo().name();
        String texto = mensagem.getMensagem();
        String conteudo = String.format("%s %s %s %s", dataHora, ip, tipoEvento, texto);
        return conteudo;
    }

    public static Mensagem deserializar(String conteudo) {
        String[] partes = conteudo.split("\\s+", 4);
        String strDataHora = partes[0];
        String strIp = partes[1];
        String strTipoEvento = partes[2];
        String strTexto = partes[3];

        ZonedDateTime dataHora = ZonedDateTime.from(getDateTimeFormatter().parse(strDataHora));
        String ip = strIp;
        TipoMensagem tipoEvento = TipoMensagem.buscarPorNome(strTipoEvento).get();
        String texto = strTexto;
        Mensagem mensagem = new Mensagem(tipoEvento, ip, dataHora, texto);
        return mensagem;
    }
}
