package uva.sd.trabalho2.servicos.impl.rabbitmq;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import uva.sd.trabalho2.modelo.Mensagem;
import uva.sd.trabalho2.modelo.RepositorioMensagem;
import uva.sd.trabalho2.servicos.ConfiguracaoMensagens;
import uva.sd.trabalho2.servicos.ConsumidorMensagem;
import uva.sd.trabalho2.servicos.ErroMensageria;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ConsumidorMensagemRabbitMq extends DefaultConsumer implements ConsumidorMensagem {

    private final ConfiguracaoMensagens configuracao;
    private final Channel channel;
    private final RepositorioMensagem repositorioMensagem;

    private final String queueName;
    private final String exchangeName;
    private final String consumerTag;

    public ConsumidorMensagemRabbitMq(Channel channel, RepositorioMensagem repositorio, ConfiguracaoMensagens configuracao) {
        super(channel);
        this.configuracao = configuracao;
        this.exchangeName = this.configuracao.getNomeTopico();
        this.channel = channel;
        this.repositorioMensagem = repositorio;

        try {
            if (this.configuracao.getNomeFila().isPresent()) {
                this.queueName = this.configuracao.getNomeFila().get();
            } else {
                this.queueName = channel.queueDeclare().getQueue();
            }

            if (this.configuracao.getChavesRedirecionamento().isEmpty()) {
                this.channel.queueBind(this.queueName, this.exchangeName, "");

            } else {
                for (String bindingKey : this.configuracao.getChavesRedirecionamento()) {
                    this.channel.queueBind(this.queueName, this.exchangeName, bindingKey);
                }
            }

            this.consumerTag = this.channel.basicConsume(this.queueName, this);

        } catch (IOException e) {
            throw new ErroMensageria(e);
        }
    }

    @Override
    public void handleDelivery(
            String consumerTag,
            Envelope envelope,
            AMQP.BasicProperties properties,
            byte[] body
    ) throws IOException {
        Mensagem mensagem = MensagensRabbitMq.deserializar(body);
        repositorioMensagem.salvar(mensagem);
    }

    @Override
    public void close() {
        try {
            this.channel.close();
        } catch (IOException | TimeoutException e) {
            throw new ErroMensageria(e);
        }
    }
}
