package uva.sd.trabalho2.servicos;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class ConfiguracaoMensagens {

    public static final String NOME_TOPICO_PADRAO = "/uva-sd/canais/mensagens";

    private final String nomeFila; // Traduz diretamente para "queue name"
    private final String nomeTopico; // Traduz diretamente para "exchange name"
    private final List<String> chavesRedirecionamento; // Traduz diretamente para "binding keys"

    public ConfiguracaoMensagens() {
        this(null, NOME_TOPICO_PADRAO, Collections.emptyList());
    }

    private ConfiguracaoMensagens(String nomeFila, String nomeTopico, List<String> chavesRedirecionamento) {
        this.nomeFila = nomeFila;
        this.nomeTopico = nomeTopico;
        this.chavesRedirecionamento = chavesRedirecionamento;
    }

    public Optional<String> getNomeFila() {
        return Optional.ofNullable(nomeFila);
    }

    public String getNomeTopico() {
        return nomeTopico;
    }

    public List<String> getChavesRedirecionamento() {
        return chavesRedirecionamento;
    }

    public String getNomeTopicoBroadcast() {
        return nomeTopico + ":all";
    }

    public ConfiguracaoMensagens comNomeFila(String nomeFila) {
        return new ConfiguracaoMensagens(nomeFila, this.nomeTopico, this.chavesRedirecionamento);
    }

    public ConfiguracaoMensagens comNomeTopico(String nomeTopico) {
        return new ConfiguracaoMensagens(this.nomeFila, nomeTopico, this.chavesRedirecionamento);
    }

    public ConfiguracaoMensagens comChavesRedirecionamento(String... chavesRedirecionamento) {
        return new ConfiguracaoMensagens(this.nomeFila, this.nomeTopico, Arrays.asList(chavesRedirecionamento));
    }

    public ConfiguracaoMensagens comChavesRedirecionamento(List<String> chavesRedirecionamento) {
        return new ConfiguracaoMensagens(this.nomeFila, this.nomeTopico, chavesRedirecionamento);
    }

    public static ConfiguracaoMensagens padrao() {
        return new ConfiguracaoMensagens();
    }

}
