package uva.sd.trabalho2.servicos;

public interface ConsumidorMensagem extends AutoCloseable {

    void close();
}
