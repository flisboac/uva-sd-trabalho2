package uva.sd.trabalho2.servicos;

import uva.sd.trabalho2.modelo.Mensagem;

public interface ProdutorMensagem extends AutoCloseable {

    void enviar(Mensagem mensagem) throws ErroMensageria;
    void close();
}
