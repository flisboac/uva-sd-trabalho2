package uva.sd.trabalho2.servicos;

import uva.sd.trabalho2.lang.Erro;

public class ErroMensageria extends Erro {
    public ErroMensageria() {
    }

    public ErroMensageria(String s) {
        super(s);
    }

    public ErroMensageria(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ErroMensageria(Throwable throwable) {
        super(throwable);
    }

    public ErroMensageria(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
