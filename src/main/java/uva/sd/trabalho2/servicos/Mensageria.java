package uva.sd.trabalho2.servicos;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import uva.sd.trabalho2.modelo.RepositorioMensagem;
import uva.sd.trabalho2.modelo.TipoMensagem;
import uva.sd.trabalho2.modelo.impl.Repositorios;
import uva.sd.trabalho2.servicos.impl.rabbitmq.ConsumidorMensagemRabbitMq;
import uva.sd.trabalho2.servicos.impl.rabbitmq.ProdutorMensagemRabbitMq;
import uva.sd.trabalho2.servicos.impl.rabbitmq.ConexoesRabbitMq;

import java.io.IOException;

public class Mensageria implements AutoCloseable {

    private final Connection connection = ConexoesRabbitMq.criarConexao();
    private final ConfiguracaoMensagens configuracao = ConfiguracaoMensagens.padrao();

    public static Mensageria novo() {
        Mensageria mensageria = new Mensageria();
        return mensageria;
    }

    public ConsumidorMensagem criarConsumidorGenerico(RepositorioMensagem repositorio) {
        return new ConsumidorMensagemRabbitMq(
                criarChannel(),
                repositorio,
                configuracao);
    }

    public ConsumidorMensagem criarConsumidorPrioritario(RepositorioMensagem repositorio) {
        return new ConsumidorMensagemRabbitMq(
                criarChannel(),
                repositorio,
                configuracao.comChavesRedirecionamento(TipoMensagem.getNomesPrioritarios()));
    }

    public ProdutorMensagem criarProdutor() {
        return new ProdutorMensagemRabbitMq(
                criarChannel(),
                configuracao);
    }

    private Channel criarChannel() {
        try {
            Channel channel = this.connection.createChannel();
            //channel.exchangeDeclare(configuracao.getNomeTopicoBroadcast(), BuiltinExchangeType.FANOUT);
            channel.exchangeDeclare(configuracao.getNomeTopico(), BuiltinExchangeType.DIRECT);
            return channel;
        } catch (IOException e) {
            throw new ErroMensageria(e);
        }
    }

    @Override
    public void close() {
        try {
            this.connection.close();
        } catch (IOException e) {
            throw new ErroMensageria(e);
        }
    }
}
